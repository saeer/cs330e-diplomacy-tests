from io import StringIO
from unittest import main, TestCase
from Diplomacy import Army, diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

class TestDiplomacy(TestCase):
    def test_read_1(self):
        s = "A Madrid Hold\n"
        army = diplomacy_read(s)
        self.assertEqual(army.name, "A")
        self.assertEqual(army.location, "Madrid")
        self.assertEqual(army.action, "Hold")

    def test_read_2(self):
        s = "B Paris Move Tokyo\n"
        army = diplomacy_read(s)
        self.assertEqual(army.name, "B")
        self.assertEqual(army.location, "Paris")
        self.assertEqual(army.action, "Move")
        self.assertEqual(army.target, "Tokyo")
        
    def test_read_3(self):
        s = "Z Bangkok Support Shanghai\n"
        army = diplomacy_read(s)
        self.assertEqual(army.name, "Z")
        self.assertEqual(army.location, "Bangkok")
        self.assertEqual(army.action, "Support")
        self.assertEqual(army.target, "Shanghai")

    def test_eval_1(self):
        armies = [
            Army("A", "Madrid", "Hold"),
            Army("B", "Paris", "Move", "Madrid"),
            Army("K", "Bangkok", "Move", "Paris"),
            Army("Z", "Shanghai", "Move", "Bangkok")
        ]
        diplomacy_eval(armies)
        self.assertEqual(armies[0].location, "[dead]")
        self.assertEqual(armies[1].location, "[dead]")
        self.assertEqual(armies[2].location, "Paris")
        self.assertEqual(armies[3].location, "Bangkok")

    def test_eval_2(self):
        armies = [
            Army("A", "Madrid", "Hold"),
            Army("B", "Boston", "Move", "Madrid"),
            Army("C", "Chicago", "Move", "Madrid"),
            Army("D", "Detroit", "Move", "Madrid"),
            Army("E", "ElPaso", "Support", "B")
        ]
        diplomacy_eval(armies)
        self.assertEqual(armies[0].location, "[dead]")
        self.assertEqual(armies[1].location, "Madrid")
        self.assertEqual(armies[2].location, "[dead]")
        self.assertEqual(armies[3].location, "[dead]")
        self.assertEqual(armies[4].location, "ElPaso")

    def test_eval_3(self):
        armies = [
            Army("A", "Madrid", "Hold"),
            Army("B", "Boston", "Move", "Madrid"),
            Army("C", "Chicago", "Move", "Madrid"),
            Army("D", "Detroit", "Support", "A"),
            Army("E", "ElPaso", "Support", "A")
        ]
        diplomacy_eval(armies)
        self.assertEqual(armies[0].location, "Madrid")
        self.assertEqual(armies[1].location, "[dead]")
        self.assertEqual(armies[2].location, "[dead]")
        self.assertEqual(armies[3].location, "Detroit")
        self.assertEqual(armies[4].location, "ElPaso")
    
    def test_eval_4(self):
        armies = [
            Army("A", "Madrid", "Hold"),
            Army("B", "Boston", "Move", "Madrid"),
            Army("C", "Chicago", "Move", "Madrid"),
            Army("D", "Detroit", "Support", "A"),
            Army("E", "ElPaso", "Support", "B")
        ]
        diplomacy_eval(armies)
        self.assertEqual(armies[0].location, "[dead]")
        self.assertEqual(armies[1].location, "[dead]")
        self.assertEqual(armies[2].location, "[dead]")
        self.assertEqual(armies[3].location, "Detroit")
        self.assertEqual(armies[4].location, "ElPaso")

    def test_eval_5(self):
        armies = [
            Army("A", "Edmonton", "Move", "Austin"),
            Army("B", "Austin", "Move", "Edmonton"),
            Army("C", "Mimbres", "Move", "Pluto"),
            Army("D", "McAllen", "Move", "Mimbres"),
            Army("E", "Mission", "Support", "D"),
            Army("F", "Brownsville", "Support" "E"),
        ]
        diplomacy_eval(armies)
        self.assertEqual(armies[0].location, "Austin")
        self.assertEqual(armies[1].location, "Edmonton")
        self.assertEqual(armies[2].location, "Pluto")
        self.assertEqual(armies[3].location, "Mimbres")
        self.assertEqual(armies[4].location, "Mission")
        self.assertEqual(armies[5].location, "Brownsville")
    
    # Taken from Piazza
    def test_eval_6(self):
        armies = [
            Army("A", "Tokyo", "Hold"),
            Army("B", "Beijing", "Support", "A"),
            Army("C", "London", "Support", "B"),
            Army("D", "Austin", "Move", "Beijing"),
            Army("E", "NewYork", "Move", "Tokyo"),
        ]
        diplomacy_eval(armies)
        self.assertEqual(armies[0].location, "[dead]")
        self.assertEqual(armies[1].location, "Beijing")
        self.assertEqual(armies[2].location, "London")
        self.assertEqual(armies[3].location, "[dead]")
        self.assertEqual(armies[4].location, "[dead]")
    
    # Taken from Piazza
    def test_eval_7(self):
        armies = [
            Army("A", "Austin", "Move", "SanAntonio"),
            Army("B", "Dallas", "Support", "A"),
            Army("C", "Houston", "Move", "Dallas"),
            Army("D", "SanAntonio", "Support", "C"),
            Army("E", "ElPaso", "Move", "Austin"),
            Army("F", "Waco", "Move", "Austin")
        ]
        diplomacy_eval(armies)
        self.assertEqual(armies[0].location, "[dead]")
        self.assertEqual(armies[1].location, "[dead]")
        self.assertEqual(armies[2].location, "[dead]")
        self.assertEqual(armies[3].location, "[dead]")
        self.assertEqual(armies[4].location, "[dead]")
        self.assertEqual(armies[5].location, "[dead]")
    
    def test_eval_8(self):
        armies = [
            Army("A", "Austin", "Hold")
        ]
        diplomacy_eval(armies)
        self.assertEqual(armies[0].location, "Austin")
    
    def test_print_1(self):
        w = StringIO()
        armies = [
            Army("A", "Madrid", "Hold")
        ]
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        armies = [
            Army("B", "Tokyo", None)
        ]
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "B Tokyo\n")

    def test_print_3(self):
        w = StringIO()
        armies = [
            Army("Z", "Bangkok", None)
        ]
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "Z Bangkok\n")

    def test_solve_1(self):
        r = StringIO("A Austin Hold\nB Barcelona Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Austin Hold\nB Barcelona Move Austin\n C Chicago Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Austin\nC Chicago\n")

    def test_solve_3(self):
        r = StringIO("A Austin Move Barcelona\nB Barcelona Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Austin\n")

if __name__ == "__main__": #pragma: no cover
    main()