#!/usr/bin/env python3
# -----------------
# TestDiplomacy.py
# -----------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve


# -------------
# TestDiplomacy
# -------------

class TestDiplomacy (TestCase):

    # -----
    # solve
    # -----

    def test_solve_1(self):
        reader = StringIO("A Madrid Hold\n")
        writer = StringIO()
        diplomacy_solve(reader, writer)
        self.assertEqual(writer.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        reader = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        writer = StringIO()
        diplomacy_solve(reader, writer)
        self.assertEqual(writer.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        reader = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\n")
        writer = StringIO()
        diplomacy_solve(reader, writer)
        self.assertEqual(writer.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_4(self):
        reader = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        writer = StringIO()
        diplomacy_solve(reader, writer)
        self.assertEqual(writer.getvalue(),
                         "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_5(self):
        reader = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        writer = StringIO()
        diplomacy_solve(reader, writer)
        self.assertEqual(writer.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_6(self):
        reader = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        writer = StringIO()
        diplomacy_solve(reader, writer)
        self.assertEqual(writer.getvalue(),
                         "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_7(self):
        reader = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        writer = StringIO()
        diplomacy_solve(reader, writer)
        self.assertEqual(writer.getvalue(),
                         "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_8(self):
        reader = StringIO(
            "A Madrid Move Barcelona\nB Barcelona Move Madrid\n")
        writer = StringIO()
        diplomacy_solve(reader, writer)
        self.assertEqual(writer.getvalue(), "A Barcelona\nB Madrid\n")

    def test_solve_9(self):
        reader = StringIO(
            "A Minneapolis Hold\nB Bangkok Move Minneapolis\nC Cairo Support B\n")
        writer = StringIO()
        diplomacy_solve(reader, writer)
        self.assertEqual(writer.getvalue(),
                         "A [dead]\nB Minneapolis\nC Cairo\n")

    def test_solve_10(self):
        reader = StringIO(
            "A NewYork Hold\nB Boston Move NewYork\nC Vancouver Move NewYork\nD Perth Support B\nE Dublin Support A\n")
        writer = StringIO()
        diplomacy_solve(reader, writer)
        self.assertEqual(writer.getvalue(),
                         "A [dead]\nB [dead]\nC [dead]\nD Perth\nE Dublin\n")

    def test_solve_11(self):
        reader = StringIO(
            "A Paris Hold\nB London Support A\nC Madrid Move London\nD Tokyo Move Paris\n")
        writer = StringIO()
        diplomacy_solve(reader, writer)
        self.assertEqual(writer.getvalue(),
                         "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_12(self):
        reader = StringIO(
            "A Austin Hold\nB Beijing Hold\nC Calgary Hold\nD Dublin Hold\nE Edinburgh Hold\nF Frankfurt Hold\n")
        writer = StringIO()
        diplomacy_solve(reader, writer)
        self.assertEqual(writer.getvalue(
        ), "A Austin\nB Beijing\nC Calgary\nD Dublin\nE Edinburgh\nF Frankfurt\n")

    def test_solve_13(self):
        reader = StringIO(
            "A Austin Move Paris\nB Barcelona Move Paris\nC Tokyo Move Barcelona\nD Seattle Move Tokyo\n")
        writer = StringIO()
        diplomacy_solve(reader, writer)
        self.assertEqual(writer.getvalue(),
                         "A [dead]\nB [dead]\nC Barcelona\nD Tokyo\n")
                         
# ----
# main
# ----
if __name__ == "__main__":  # pragma: no cover
    main()
