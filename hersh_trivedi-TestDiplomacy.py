from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, mili, run_game, diplomacy_print, diplomacy_solve

class TestDiplomacy(TestCase):

    '''
    unit tests for diplomacy_read()
    '''

    def test_read_1(self):
        input = ['A Austin Move Dallas']
        s = diplomacy_read(input)
        self.assertEqual(s, [['A', 'Austin', 'Move', 'Dallas']])
    
    def test_read_2(self):
        input = ['A Austin Move Dallas', 'B Dallas Hold', 'C SanAntonio Support A']
        s = diplomacy_read(input)
        self.assertIsInstance(s, list)

    '''
    corner and failure cases for diplomacy_read()
    '''

    def test_read_3(self):
        input = 'A Austin Move Dallas'
        self.assertEqual(diplomacy_read(input), 'invalid')

    def test_read_4(self):
        input = ['A Austin']
        self.assertEqual(diplomacy_read(input), 'invalid')

    def test_read_5(self):
        input = ['A Austin Hold Move Dallas']
        self.assertEqual(diplomacy_read(input), 'invalid')


    '''
    unit tests for run_game()
    '''

    def test_run_1(self):
        s = [['A', 'Austin', 'Hold'], ['B', 'Dallas', 'Move', 'SanAntonio'], ['C', 'Houston', 'Move', 'SanAntonio']]
        v = run_game(s)
        self.assertEqual(v, [['A', 'Austin'], ['B', '[dead]'], ['C', '[dead]']])

    def test_run_2(self):
        s = [['A', 'Austin', 'Move', 'Dallas'], ['B', 'Dallas', 'Hold'], ['C', 'SanAntonio', 'Support', 'A'], ['D', 'Houston', 'Move', 'Dallas']]
        v = run_game(s)
        self.assertEqual(v, [['A', '[dead]'], ['B', '[dead]'], ['C', 'SanAntonio'], ['D', '[dead]']])

    def test_run_3(self):
        s = [['A', 'Austin', 'Move', 'Dallas'], ['B', 'Dallas', 'Hold'], ['C', 'SanAntonio', 'Support', 'A']]
        v = run_game(s)
        self.assertEqual(len(v[0]), 2)
    
    def test_run_4(self):
        s = [['A', 'Austin', 'Move', 'SanAntonio'], ['B', 'Dallas', 'Move', 'SanAntonio'], ['C', 'Houston', 'Move', 'SanAntonio']]
        v = run_game(s)
        self.assertEqual(v, [['A', '[dead]'], ['B', '[dead]'], ['C', '[dead]']])

    def test_run_5(self):
        s = [['A', 'Austin', 'Support', 'B'], ['B', 'Dallas', 'Move', 'SanAntonio'], ['C', 'Houston', 'Move', 'Austin']]
        v = run_game(s)
        self.assertEqual(v, [['A', '[dead]'], ['B', 'SanAntonio'], ['C', '[dead]']])

    def test_run_6(self):
        s = [['A', 'Austin', 'Move', 'Houston'], ['B', 'Houston', 'Move', 'SanAntonio'], ['C', 'SanAntonio', 'Move', 'Austin']]
        v = run_game(s)
        self.assertEqual(v, [['A', 'Houston'], ['B', 'SanAntonio'], ['C', 'Austin']])

    def test_run_7(self):
        s = [['A', 'Austin', 'Move', 'Houston'], ['B', 'Dallas', 'Move', 'Houston'], ['C', 'SanAntonio', 'Support', 'A']]
        v = run_game(s)
        self.assertEqual(v, [['A', 'Houston'], ['B', '[dead]'], ['C', 'SanAntonio']])
    
    def test_run_8(self):
        s = [['A', 'Austin', 'Move', 'Houston'], ['B', 'Dallas', 'Move', 'Houston'], ['C', 'SanAntonio', 'Support', 'A'], ['D', 'Galveston', 'Move', 'Houston'], ['E', 'Corpus', 'Support', 'B'], ['F', 'Christi', 'Support', 'A']]
        v = run_game(s)
        self.assertEqual(v, [['A', 'Houston'], ['B', '[dead]'], ['C', 'SanAntonio'], ['D', '[dead]'], ['E', 'Corpus'], ['F', 'Christi']])

    def test_run_9(self):
        s = [['A', 'Houston', 'Hold'], ['B', 'Austin', 'Move', 'Houston'], ['C', 'Dallas', 'Move', 'Houston'], ['D', 'SanAntonio', 'Support', 'A'], ['E', 'Galveston', 'Move', 'Houston'], ['F', 'Corpus', 'Support', 'B'], ['G', 'Christi', 'Support', 'A']]
        v = run_game(s)
        self.assertEqual(v, [['A', 'Houston'], ['B', '[dead]'], ['C', '[dead]'], ['D', 'SanAntonio'], ['E', '[dead]'], ['F', 'Corpus'], ['G', 'Christi']])

    '''
    corner and failure cases for run_game()
    '''

    def test_run_10(self):
        s = [['A', 'San', 'Antonio', 'Hold']]
        v = run_game(s)
        self.assertEqual(v, 'invalid')

    def test_run_11(self):
        s = [['A', 'New', 'Braunfels', 'Hold']]
        v = run_game(s)
        self.assertEqual(v, 'invalid')

    '''
    unit tests for diplomacy_print()
    '''

    def test_print_1(self):
        w = StringIO()
        v = [['A', 'Dallas']]
        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), 'A Dallas\n')
    
    def test_print_2(self):
        w = StringIO()
        v = [['A', 'Dallas'], ['B', '[dead]'], ['C', 'SanAntonio']]
        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), 'A Dallas\nB [dead]\nC SanAntonio\n')
    
    def test_print_3(self):
        w = StringIO()
        v = [['A', 'Dallas'], ['B', '[dead]'], ['C', 'SanAntonio']]
        diplomacy_print(w, v)
        self.assertIsInstance(w.getvalue(), str)

    '''
    corner and failure cases for diplomacy_print
    '''

    def test_print_4(self):
        w = StringIO()
        v = [['A'], ['B'], ['C']]
        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), 'invalid')
        
    def test_print_5(self):
        w = StringIO()
        v = [['A San Antonio'], ['B New York']]
        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), 'invalid')

    '''
    unit tests for diplomacy_solve()
    '''

    def test_solve_1(self):
        r = StringIO('A Austin Move Dallas\nB Dallas Hold\nC SanAntonio Support A')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Dallas\nB [dead]\nC SanAntonio\n')

    '''
    corner and failure cases for diplomacy_solve()
    '''

    def test_solve_2(self):
        r = StringIO('A San Antonio Move Austin\nB New Braunfels Support San Antonio\nC Austin Hold')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'invalid')

    def test_solve_3(self):
        r = StringIO('A San Antonio Hold\nB New Braunfels Hold\nC Austin Hold')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'invalid')

if __name__ == "__main__":  #pragma: no cover
    main()